# HCC PHEI Public Health Analyst R Analysis Assessment - **↓ Read Below ↓**

**Please read this before starting**

## Task

We have been asked to produce a summary of COVID-19 cases in Hertfordshire using data on diagnosed cases. This analysis should summarise what is going on in regards to COVID-19 cases currently to help inform whether interventions are needed and if so where.

Perform descriptive analysis on COVID-19 cases in Hertfordshire and the districts within Hertfordshire using R, exploring trends and differences between areas. Conduct any analyses and make any visualisations you consider appropriate to answer the request.

Produce a short (a max of around 2 page including figures) report summarising your findings to discuss during the interview.

**You have 90 minutes to complete this task and return the code and any outputs.**

### Data 

Data on COVID-19 cases can be found here:

https://gitlab.com/wyuill/ph_analyst_assessment/-/raw/master/coronavirus-cases_latest.csv?inline=false 

A supporting csv file containing the names, area codes, and populations of Hertfordshire, the districts within Hertfordshire, as well as nearby areas, is provided here:

https://gitlab.com/wyuill/ph_analyst_assessment/-/blob/master/supporting_data.csv?inline=false 

## Additional Details

### Expected output
- An R script containing the analysis of the data so that your results can be reproduced. This task should only require common R packages.

- A short (a max of around 2 page including figures) report containing:
  - A summary of the results of your analyses in 2-3 sentences
  - Any analysis, charts or tables relevant to your conclusions 
  - A list of the 3 main limitations of the data
  - Suggestions of how the analysis could be refined

This report can be in whatever form you consider most appropriate (e.g. an Rmarkdown report or Word document). Note reproducibility is a grading criterion.

Return the code and any outputs at the end of the task.

Be prepared to talk through your findings during the interview.

### What we are assessing
We will be assessing output and code on the following:

1. Clarity of presentation
2. Clarity of code
3. Reproducibility / ability to update findings with new data
4. Accuracy
5. Depth of analysis

There are several ways you could approach this task. You are not expected to do all of them but you should be prepared to talk about what else you would have done if you had the time and resources.

### About the data
This data contains the number of people with a lab-confirmed positive COVID-19 test by the date the test was taken and where the test has been processed in time for publication. Tests normally require booking in advance.

COVID-19 cases are identified by taking specimens from people and sending these specimens to laboratories around the UK for swab testing. If the test is positive, this is a referred to as a lab-confirmed case. Data is deduplicated by matching personal details so that if a person has had more than one positive test they are only counted as one case.

Cases are allocated to the person's area of residence based on their NHS record.
